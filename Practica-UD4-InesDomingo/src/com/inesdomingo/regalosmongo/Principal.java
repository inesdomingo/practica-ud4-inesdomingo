package com.inesdomingo.regalosmongo;


import com.inesdomingo.regalosmongo.gui.Controlador;
import com.inesdomingo.regalosmongo.gui.Modelo;
import com.inesdomingo.regalosmongo.gui.Vista;

/**
 * Created by DAM on 18/02/2022.
 */
public class Principal {
    public static void main(String[] args) {
        Vista vista=new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
