package com.inesdomingo.regalosmongo.base;

import org.bson.types.ObjectId;

import java.sql.Date;
import java.time.LocalDate;

public class Tienda {
    private ObjectId _id;
    private String nombre;
    private LocalDate fechaApertura;
    private boolean abierto;
    private String telefono;
    private String ciudad;


    public Tienda( String nombre, LocalDate fechaApertura, boolean abierto, String telefono, String ciudad) {

        this.nombre = nombre;
        this.fechaApertura = fechaApertura;
        this.abierto = abierto;
        this.telefono = telefono;
        this.ciudad = ciudad;
    }

    public Tienda() {
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public boolean isAbierto() {
        return abierto;
    }

    public void setAbierto(boolean abierto) {
        this.abierto = abierto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return
                " nombre='" + nombre ;
    }
}
