package com.inesdomingo.regalosmongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Pedido {
    private ObjectId _id;
    private double precio;
    private boolean internet;
    private boolean pagado;
    private LocalDate fechaPedido;

    public Pedido(float precio, boolean internet,boolean pagado, LocalDate fechaPedido){
        this.precio=precio;
        this.internet=internet;
        this.pagado=pagado;
        this.fechaPedido=fechaPedido;

    }

    public Pedido() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public boolean isInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public LocalDate getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(LocalDate fechaPedido) {
        this.fechaPedido = fechaPedido;
    }


    @Override
    public String toString() {
        return "Pedido{" +
                " precio=" + precio +
                ", internet=" + internet +
                ", pagado=" + pagado +
                ", fechaPedido=" + fechaPedido +
                '}';
    }
}
