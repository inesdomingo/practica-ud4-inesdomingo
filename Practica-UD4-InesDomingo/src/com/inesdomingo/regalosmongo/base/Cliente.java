package com.inesdomingo.regalosmongo.base;

import org.bson.types.ObjectId;
import org.omg.PortableInterceptor.LOCATION_FORWARD;

import java.sql.Date;
import java.time.LocalDate;

public class Cliente {
    private ObjectId _id;
    private String nombre;
    private String apellido;
    private LocalDate fechaNacimiento;
    private String ciudad;
    private boolean pedido;

    public Cliente( String nombre, String apellido, LocalDate fechaNacimiento, String ciudad, boolean pedido) {

        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
        this.ciudad = ciudad;
        this.pedido = pedido;
    }

    public Cliente() {
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public boolean isPedido() {
        return pedido;
    }

    public void setPedido(boolean pedido) {
        this.pedido = pedido;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
