package com.inesdomingo.regalosmongo.gui;


import com.inesdomingo.regalosmongo.base.*;
import com.inesdomingo.regalosmongo.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ines Domingo el dia 01/03/2022.
 */
public class Modelo {
    private final static String COLECCION_CLIENTES = "Clientes";
    private final static String COLECCION_REGALOS = "Regalos";
    private final static String COLECCION_EMPLEADOS = "Empleados";
    private final static String COLECCION_PEDIDO = "Pedido";
    private final static String COLECCION_TIENDA = "Tienda";
    private final static String DATABASE="Tienda_Regalos";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection<Document> coleccionClientes;
    private MongoCollection<Document> coleccionRegalos;
    private MongoCollection<Document> coleccionEmpleados;
    private MongoCollection<Document> coleccionPedido;
    private MongoCollection<Document> coleccionTienda;

    public void conectar() {
        client=new MongoClient();
        baseDatos=client.getDatabase(DATABASE);
        coleccionClientes=baseDatos.getCollection(COLECCION_CLIENTES);
        coleccionRegalos=baseDatos.getCollection(COLECCION_REGALOS);
        coleccionEmpleados=baseDatos.getCollection(COLECCION_EMPLEADOS);
        coleccionPedido=baseDatos.getCollection(COLECCION_PEDIDO);
        coleccionTienda=baseDatos.getCollection(COLECCION_TIENDA);
    }

    public void desconectar() {
        client.close();
    }


    public void altaCliente(String nombre, String apellido, LocalDate fecha_nacimiento, String ciudad, boolean pedido ){

        Cliente cliente=new Cliente(nombre,apellido, fecha_nacimiento,ciudad,pedido);
        coleccionClientes.insertOne(clienteToDocument(cliente));

    }

    public void modificarCliente(Cliente cliente){
        coleccionClientes.replaceOne(new Document("_id",cliente.get_id()),
                clienteToDocument(cliente));

    }

    public void eliminarCliente(Cliente cliente){
        coleccionClientes.deleteOne(clienteToDocument(cliente));

    }
    public void altaRegalo(String tipo_regalo,float precio,boolean escrito_nombre,boolean escrito_fecha,String idioma){

        Regalo regalo=new Regalo(tipo_regalo,idioma,precio,escrito_nombre,escrito_fecha);
        coleccionRegalos.insertOne(regaloToDocument(regalo));

    }

    public void modificarRegalo(Regalo regalo){
        coleccionRegalos.replaceOne(new Document("_id",regalo.get_id()),
                regaloToDocument(regalo));

    }

    public void eliminarRegalo(Regalo regalo){
        coleccionRegalos.deleteOne(regaloToDocument(regalo));

    }
    public void altaPedido(float precio, boolean internet, boolean pagado, LocalDate fecha_pedido){

        Pedido pedido=new Pedido(precio,internet,pagado,fecha_pedido);
        coleccionPedido.insertOne(PedidoToDocument(pedido));

    }

    public void modificarPedido(Pedido pedido){
        coleccionPedido.replaceOne(new Document("_id",pedido.get_id()),
                PedidoToDocument(pedido));

    }

    public void eliminarPedido(Pedido pedido){
        coleccionPedido.deleteOne(PedidoToDocument(pedido));

    }

    public void altaEmpleado(String nombre, int edad, LocalDate fechaInicio, Tienda tienda) {
        Empleado empleado = new Empleado(nombre, edad, fechaInicio,tienda);
        coleccionEmpleados.insertOne(EmpleadoToDocument(empleado));

    }

    public void modificarEmpleado(Empleado empleado){
        coleccionEmpleados.replaceOne(new Document("_id",empleado.get_id()),
                EmpleadoToDocument(empleado));

    }

    public void eliminarEmpleado(Empleado empleado){
        coleccionEmpleados.deleteOne(EmpleadoToDocument(empleado));
    }
    public void altaTienda(String nombre, LocalDate fechaApertura, boolean abierto,String telefono, String ciudad){

        Tienda tienda=new Tienda( nombre,fechaApertura,abierto,telefono,ciudad);
        coleccionTienda.insertOne(TiendaToDocument(tienda));

    }

    public void modificarTienda(Tienda tienda){
        coleccionTienda.replaceOne(new Document("_id",tienda.get_id()),
                TiendaToDocument(tienda));
    }

    public void eliminarTienda(Tienda tienda){
        coleccionTienda.deleteOne(TiendaToDocument(tienda));

    }
    //GET
    //CLIENTE
    public ArrayList<Cliente> getCliente() {
        ArrayList<Cliente> lista = new ArrayList<>();

        for (Document document : coleccionClientes.find()) {
            lista.add(documentToCliente(document));
        }
        return lista;
    }

    public ArrayList<Cliente> getCliente(String comparador) {
        ArrayList<Cliente> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("fecha nacimiento", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("ciudad", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("pedido", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : coleccionClientes.find(query)) {
            lista.add(documentToCliente(document));
        }

        return lista;
    }
    //TIENDA
    public ArrayList<Tienda> getTienda() {
        ArrayList<Tienda> lista = new ArrayList<>();

        for (Document document : coleccionTienda.find()) {
            lista.add(documentToTienda(document));
        }
        return lista;
    }
    public ArrayList<Tienda> getTienda(String comparador) {
        ArrayList<Tienda> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("fecha_apertura", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("abierto", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("ciudad", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("telefono", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : coleccionTienda.find(query)) {
            lista.add(documentToTienda(document));
        }

        return lista;
    }
    //REGALO
    public ArrayList<Regalo> getRegalo() {
        ArrayList<Regalo> lista = new ArrayList<>();

        for (Document document : coleccionRegalos.find()) {
            lista.add(documentToRegalo(document));
        }
        return lista;
    }
    public ArrayList<Regalo> getRegalo(String comparador) {
        ArrayList<Regalo> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("tipo", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("idioma", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("precio", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("fecha?", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("nombre?", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : coleccionRegalos.find(query)) {
            lista.add(documentToRegalo(document));
        }

        return lista;
    }
    //EMPLEADO
    public ArrayList<Empleado> getEmpleado() {
        ArrayList<Empleado> lista = new ArrayList<>();

        for (Document document : coleccionEmpleados.find()) {
            lista.add(documentToEmpleado(document));
        }
        return lista;
    }

    public ArrayList<Empleado> getEmpleado(String comparador) {
        ArrayList<Empleado> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("edad", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("fecha inicio", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("Tienda", new Document("$regex", "/*" + comparador + "/*")));

        query.append("$or", listaCriterios);

        for (Document document : coleccionEmpleados.find(query)) {
            lista.add(documentToEmpleado(document));
        }

        return lista;
    }
    //EMPLEADO
    public ArrayList<Pedido> getPedidos() {
        ArrayList<Pedido> lista = new ArrayList<>();

        for (Document document : coleccionPedido.find()) {
            lista.add(documentToPedido(document));
        }
        return lista;
    }

    public ArrayList<Pedido> getPedidos(String comparador) {
        ArrayList<Pedido> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("precio", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("internet?", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("pagado?", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("fecha pedido", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("Regalo", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("Cliente", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("Empleado", new Document("$regex", "/*" + comparador + "/*")));

        query.append("$or", listaCriterios);

        for (Document document : coleccionPedido.find(query)) {
            lista.add(documentToPedido(document));
        }

        return lista;
    }


    // REGALOS
    public Document regaloToDocument(Regalo unRegalo) {
        Document documento = new Document();
        documento.append("tipo",unRegalo.getTiporegalo());
        documento.append("idioma",unRegalo.getIdioma());
        documento.append("nombre?",unRegalo.isNombreCB());
        documento.append("fecha?", unRegalo.isFechaCB());
        documento.append("precio",unRegalo.getPrecio());
        return documento;
    }

    public Regalo documentToRegalo(Document document) {
        Regalo unRegalo=new Regalo();
        unRegalo.set_id(document.getObjectId("_id"));
        unRegalo.setTiporegalo(document.getString("tipo"));
        unRegalo.setIdioma(document.getString("idioma"));
        unRegalo.setPrecio(document.getDouble("precio"));
        unRegalo.setNombreCB(document.getBoolean("nombre?"));
        unRegalo.setFechaCB(document.getBoolean("fecha?"));
        return unRegalo;
    }
    // CLIENTE
    public Document clienteToDocument(Cliente unCliente) {
        Document documento = new Document();
        documento.append("nombre",unCliente.getNombre());
        documento.append("apellido",unCliente.getApellido());
        documento.append("fecha nacimiento",Util.formatearFecha(unCliente.getFechaNacimiento()));
        documento.append("ciudad", unCliente.getCiudad());
        documento.append("pedido",unCliente.isPedido());
        return documento;
    }

    public Cliente documentToCliente(Document document) {
        Cliente unCliente=new Cliente();
        unCliente.set_id(document.getObjectId("_id"));
        unCliente.setNombre(document.getString("nombre"));
        unCliente.setApellido(document.getString("apellido"));
        unCliente.setFechaNacimiento(Util.parsearFecha(document.getString("fecha nacimiento")));
        unCliente.setCiudad(document.getString("ciudad"));
        unCliente.setPedido(document.getBoolean("pedido"));
        return unCliente;
    }
    // TIENDA
    public Document TiendaToDocument(Tienda unaTienda) {
        Document documento = new Document();
        documento.append("nombre",unaTienda.getNombre());
        documento.append("fecha apertura",Util.formatearFecha(unaTienda.getFechaApertura()));
        documento.append("abierto?",unaTienda.isAbierto());
        documento.append("telefono",unaTienda.getTelefono());
        documento.append("ciudad", unaTienda.getCiudad());
        return documento;
    }

    public Tienda documentToTienda(Document document) {
        Tienda unaTienda=new Tienda();
        unaTienda.set_id(document.getObjectId("_id"));
        unaTienda.setNombre(document.getString("nombre"));
        unaTienda.setAbierto(document.getBoolean("abierto?"));
        unaTienda.setFechaApertura(Util.parsearFecha(document.getString("fecha apertura")));
        unaTienda.setTelefono(document.getString("telefono"));
        unaTienda.setCiudad(document.getString("ciudad"));

        return unaTienda;
    }
    // EMPLEADO
    public Document EmpleadoToDocument(Empleado unEmpleado) {
        Document documento = new Document();
        documento.append("nombre",unEmpleado.getNombre());
        documento.append("edad",unEmpleado.getEdad());
        documento.append("fecha inicio",Util.formatearFecha(unEmpleado.getFechaInicio()));
        return documento;
    }

    public Empleado documentToEmpleado(Document document) {
        Empleado unEmpleado=new Empleado();
        unEmpleado.set_id(document.getObjectId("_id"));
        unEmpleado.setNombre(document.getString("nombre"));
        unEmpleado.setEdad(document.getInteger("edad"));
        unEmpleado.setFechaInicio(Util.parsearFecha(document.getString("fecha inicio")));
        return unEmpleado;
    }
    // PEDIDO
    public Document PedidoToDocument(Pedido unPedido) {
        Document documento = new Document();
        documento.append("precio",unPedido.getPrecio());
        documento.append("internet?",unPedido.isInternet());
        documento.append("pagado?",unPedido.isPagado());
        documento.append("fecha pedido",Util.formatearFecha(unPedido.getFechaPedido()));
        return documento;
    }

    public Pedido documentToPedido(Document document) {
        Pedido unPedido=new Pedido();
        unPedido.set_id(document.getObjectId("_id"));
        unPedido.setPrecio(document.getDouble("precio"));
        unPedido.setInternet(document.getBoolean("internet?"));
        unPedido.setPagado(document.getBoolean("pagado?"));
        unPedido.setFechaPedido(Util.parsearFecha(document.getString("fecha pedido")));
        return unPedido;
    }




}
