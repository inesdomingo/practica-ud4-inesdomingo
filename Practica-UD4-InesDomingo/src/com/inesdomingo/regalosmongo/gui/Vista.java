package com.inesdomingo.regalosmongo.gui;

import com.github.lgooddatepicker.components.DatePicker;

import com.inesdomingo.regalosmongo.base.*;

import javax.swing.*;

/**
 * Created by Ines Domingo el dia 01/03/2022.
 */
public class Vista {
    private JPanel panel1;



    JTabbedPane tabbedPane1;
    JTextField tipoTxt;
    JRadioButton espanyolRB;
    JRadioButton inglesRB;
    JList listregalos;
    JButton eliminarRegaloBtn;
     JButton altaRegaloBtn;
    JButton modificarRegaloBtn;
     JTextField precioTxtRe;
     JCheckBox nombreCB;
    JCheckBox fechaCB;
    JTextField nombreTxt;
    JTextField apellidoTxt;
     JTextField CiudadTxt;
    JList listClientes;
    JButton eliminarClienteBtn;
     JButton modificarClienteBtn;
     JCheckBox pedido;
     JButton altaClienteBtn;
    DatePicker fecha_Cliente;
    JList listPedidos;
    JButton eliminarPedidoBtn;
    JButton altaPedidoBtn;
    JButton modificarPedidoBtn;
    JTextField preciotxt;
    JCheckBox internet;
    JCheckBox pagado;
    DatePicker fecha_pedido;
    JTextField nombreEmpleado;
    JTextField edadEmpleadoTxt;
    JList listEmpleados;
    JButton eliminarEmpleadoBtn;
    JButton modificarEmpleadoBtn;
    JButton altaEmpleadoBtn;
    DatePicker fecha_empleado;
    JComboBox TiendaCB;
    JTextField nombreTienda;
    JList listTienda;
    JButton eliminarTiendaBtn;
    JButton modificarTiendBtn;
    JButton altaTiendaBtn;
     DatePicker fecha_apertura;
     JCheckBox abierto;
     JTextField telefono_tiendaTxt;
     JTextField ciudad_tiendaTxt;
     JList listEmpleadosTienda;
     JButton buscarTiendaBTN;
     JTextField buscarTiendaTxt;
    JTextField buscarEmpleadoTXT;
    DefaultListModel<Regalo> dlmRegalo;
    DefaultListModel<Cliente> dlmCliente;
    DefaultListModel<Pedido> dlmPedido;
    DefaultListModel<Empleado> dlmEmpleado;
    DefaultListModel<Tienda> dlmTienda;
    private void iniciarModelos() {
        dlmRegalo = new DefaultListModel<Regalo>();
        dlmCliente = new DefaultListModel<Cliente>();
        dlmPedido = new DefaultListModel<Pedido>();
        dlmEmpleado = new DefaultListModel<Empleado>();
        dlmTienda = new DefaultListModel<Tienda>();
        listClientes.setModel(dlmCliente);
        listregalos.setModel(dlmRegalo);
        listPedidos.setModel(dlmPedido);
        listEmpleados.setModel(dlmEmpleado);
        listTienda.setModel(dlmTienda);
    }
    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        iniciarModelos();

    }
}
