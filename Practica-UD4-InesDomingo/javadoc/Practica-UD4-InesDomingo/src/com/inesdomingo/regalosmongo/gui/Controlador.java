package com.inesdomingo.regalosmongo.gui;

import com.inesdomingo.regalosmongo.base.*;

import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;


public class Controlador implements ActionListener, ListSelectionListener,KeyListener, WindowListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        modelo.conectar();
        this.vista = vista;
        this.modelo = modelo;
        listarTodo();
        anadirActionListeners(this);
        anadirListSeletionListeners(this);

    }

    /**
     * Vinculo un listener con los elementos graficos
     * @param listener el listener
     */
    private void anadirActionListeners(ActionListener listener){
        vista.altaClienteBtn.addActionListener(listener);
        vista.altaRegaloBtn.addActionListener(listener);
        vista.altaPedidoBtn.addActionListener(listener);
        vista.altaEmpleadoBtn.addActionListener(listener);
        vista.altaTiendaBtn.addActionListener(listener);

        vista.eliminarClienteBtn.addActionListener(listener);
        vista.eliminarRegaloBtn.addActionListener(listener);
        vista.eliminarPedidoBtn.addActionListener(listener);
        vista.eliminarEmpleadoBtn.addActionListener(listener);
        vista.eliminarTiendaBtn.addActionListener(listener);

        vista.modificarClienteBtn.addActionListener(listener);
        vista.modificarRegaloBtn.addActionListener(listener);
        vista.modificarPedidoBtn.addActionListener(listener);
        vista.modificarTiendBtn.addActionListener(listener);
        vista.modificarEmpleadoBtn.addActionListener(listener);

        vista.buscarTiendaBTN.addActionListener(listener);

    }

    /**
     * Vinculo un listener con los elementos graficos
     * @param listener el listener
     */
    private void anadirListSeletionListeners(ListSelectionListener listener){
        vista.listregalos.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listPedidos.addListSelectionListener(listener);
        vista.listEmpleados.addListSelectionListener(listener);
        vista.listTienda.addListSelectionListener(listener);
        vista.listEmpleadosTienda.addListSelectionListener(listener);

    }


    /**
     * Vinculo un listemer con los elementos graficos
     */
    private void anadirChangeListeners(ChangeListener listener){

    }
    private void addKeyListeners(KeyListener listener){

        vista.buscarTiendaTxt.addKeyListener(listener);
        vista.buscarEmpleadoTXT.addKeyListener(listener);
    }

    /**
     * Método que responde a un evento ActionEvent sobre los botones
     * @param e el evento lanzado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando){
            case "AltaCliente":{
                modelo.altaCliente(vista.nombreTxt.getText(),vista.apellidoTxt.getText(),vista.fecha_Cliente.getDate(),vista.CiudadTxt.getText(),vista.pedido.isSelected());
                listarTodo();
                break;
            }
            case "ModificarCliente":{
                if(!vista.listClientes.isSelectionEmpty()){
                    Cliente cliente = (Cliente) vista.listClientes.getSelectedValue();
                    modificarCliente(cliente);
                    modelo.modificarCliente(cliente);
                    listarTodo();


                }else{
                    System.out.println("selecciona un cliente");
                }
            }
            break;
            case "EliminarCliente":{
                if(!vista.listClientes.isSelectionEmpty()) {
                    Cliente cliente = (Cliente) vista.listClientes.getSelectedValue();
                    vista.dlmCliente.removeElement(cliente);
                    modelo.eliminarCliente(cliente);
                    listarTodo();

                }else{
                    System.out.println("selecciona un cliente");
                }
            }
            break;
            case "AltaRegalo":{
                if(vista.espanyolRB.isSelected()) {
                    modelo.altaRegalo(vista.tipoTxt.getText(), Float.parseFloat(vista.precioTxtRe.getText()), vista.nombreCB.isSelected(), vista.fechaCB.isSelected(),"espanyol");
                    listarTodo();
                }
                else if(vista.inglesRB.isSelected()){
                    modelo.altaRegalo(vista.tipoTxt.getText(), Float.parseFloat(vista.precioTxtRe.getText()), vista.nombreCB.isSelected(), vista.fechaCB.isSelected(),"ingles" );
                    listarTodo();
                }
                break;
            }

            case "ModificarRegalo":{
                if(!vista.listregalos.isSelectionEmpty()){
                    Regalo regalo = (Regalo) vista.listregalos.getSelectedValue();
                    modificarRegalo(regalo);
                    modelo.modificarRegalo(regalo);
                    listarTodo();

                }else{
                    System.out.println("selecciona un regalo");
                }
            }
            break;
            case "EliminarRegalo":{
                if(!vista.listregalos.isSelectionEmpty()) {
                    Regalo regalo = (Regalo) vista.listregalos.getSelectedValue();
                    vista.dlmRegalo.removeElement(regalo);
                    modelo.eliminarRegalo(regalo);
                    listarTodo();

                }else{
                    System.out.println("selecciona un regalo");
                }
            }
            break;
            case "altaEmpleado":{
                modelo.altaEmpleado(vista.nombreEmpleado.getText(),Integer.parseInt(vista.edadEmpleadoTxt.getText()),vista.fecha_empleado.getDate(),(Tienda)vista.TiendaCB.getItemAt(vista.TiendaCB.getSelectedIndex()));
                listarTodo();
                break;
            }

            case "modificarEmpleado":{
                if(!vista.listEmpleados.isSelectionEmpty()){
                    Empleado empleado = (Empleado) vista.listEmpleados.getSelectedValue();
                    modificarEmpleado(empleado);
                    modelo.modificarEmpleado(empleado);
                    listarTodo();


                }else{
                    System.out.println("selecciona un empleado");
                }
            }
            break;
            case "EliminarEmpleado":{
                if(!vista.listEmpleados.isSelectionEmpty()) {
                    Empleado empleado = (Empleado) vista.listEmpleados.getSelectedValue();
                    vista.dlmEmpleado.removeElement(empleado);
                    modelo.eliminarEmpleado(empleado);
                    listarTodo();

                }else{
                    System.out.println("selecciona un empleado");
                }
            }
            break;
            case "AltaPedido":{
                modelo.altaPedido(Float.parseFloat(vista.preciotxt.getText()),vista.internet.isSelected(),vista.pagado.isSelected(),vista.fecha_pedido.getDate());
                listarTodo();
                break;
            }

            case "ModificarPedido":{
                if(!vista.listPedidos.isSelectionEmpty()){
                    Pedido pedido  = (Pedido) vista.listPedidos.getSelectedValue();
                    modificarPedido(pedido);
                    modelo.modificarPedido(pedido);
                    listarTodo();


                }else{
                    System.out.println("selecciona un pedido");
                }
            }
            break;
            case "EliminarPedido":{
                if(!vista.listPedidos.isSelectionEmpty()) {
                    Pedido pedido = (Pedido) vista.listPedidos.getSelectedValue();
                    vista.dlmPedido.removeElement(pedido);
                    modelo.eliminarPedido(pedido);
                    listarTodo();

                }else{
                    System.out.println("selecciona un pedido");
                }
            }
            break;
            case "AltaTienda":{
                modelo.altaTienda(vista.nombreTienda.getText(),vista.fecha_apertura.getDate(),vista.abierto.isSelected(),vista.telefono_tiendaTxt.getText(), vista.ciudad_tiendaTxt.getText());
               listarTodo();
                break;
            }

            case "ModificarTienda":{
                if(!vista.listTienda.isSelectionEmpty()){
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    modificarTienda(tienda);
                    modelo.modificarTienda(tienda);
                    listarTodo();

                }else{
                    System.out.println("selecciona una tienda,nose que esta pasando");
                }
            }
            break;
            case "eliminarTienda":{
                if(!vista.listTienda.isSelectionEmpty()) {
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    vista.dlmTienda.removeElement(tienda);
                    modelo.eliminarTienda(tienda);
                    listarTodo();

                }else{
                    System.out.println("selecciona un tienda");
                }
            }
            break;

        }

        listarTodo();

    }
    public void listarTodo(){
        listarRegalo();
        listarTienda();
        listarPedido();
        listarEmpleado();
        listarCliente();
        listarTiendaComboBox(modelo.getTienda());
    }


    public void modificarRegalo(Regalo regalo) {
        regalo.setTiporegalo(vista.tipoTxt.getText());
        regalo.setPrecio(Double.valueOf(vista.precioTxtRe.getText()));
        regalo.setNombreCB(vista.nombreCB.isSelected());
        regalo.setFechaCB(vista.fechaCB.isSelected());
        if(vista.inglesRB.isSelected()){
            regalo.setIdioma("ingles");
        }
        else if(vista.espanyolRB.isSelected()){
            regalo.setIdioma("espanyol");
        }
    }

    /**
     * Modifico al cliente a partir de los datos de los campos del cliente
     * @param cliente el cliente seleccionado
     */
    public void modificarCliente(Cliente cliente){
       cliente.setNombre(vista.nombreTxt.getText());
       cliente.setApellido(vista.apellidoTxt.getText());
       cliente.setFechaNacimiento(vista.fecha_Cliente.getDate());
       cliente.setCiudad(vista.CiudadTxt.getText());
       cliente.setPedido(vista.pedido.isSelected());
    }
    /**
     * Modifico el pedido a partir de los datos de los campos del pedido
     * @param pedido el pedido seleccionado
     */
    public void modificarPedido(Pedido pedido){
        pedido.setFechaPedido(vista.fecha_pedido.getDate());
        pedido.setInternet(vista.internet.isSelected());
        pedido.setPagado(vista.pagado.isSelected());
        pedido.setPrecio(Double.parseDouble(vista.preciotxt.getText()));


    }
    public void modificarEmpleado(Empleado empleado){
        empleado.setEdad(Integer.parseInt(vista.edadEmpleadoTxt.getText()));
        empleado.setNombre(vista.nombreEmpleado.getText());
        empleado.setFechaInicio(vista.fecha_empleado.getDate());
    }

    public void modificarTienda(Tienda tienda){
        tienda.setAbierto(vista.abierto.isSelected());
        tienda.setCiudad(vista.ciudad_tiendaTxt.getText());
        tienda.setFechaApertura(vista.fecha_apertura.getDate());
        tienda.setTelefono(vista.telefono_tiendaTxt.getText());
        tienda.setNombre(vista.nombreTienda.getText());
    }
    /**
     * Listo las regalos
     */
    private void listarRegalo(){
        vista.dlmRegalo.clear();
        ArrayList<Regalo> lista = modelo.getRegalo();
        for(Regalo regalo: lista){
            vista.dlmRegalo.addElement(regalo);
        }
    }
    private void listarTienda(){
        vista.dlmTienda.clear();
        ArrayList<Tienda> lista = modelo.getTienda();
        for(Tienda tienda : lista){
            vista.dlmTienda.addElement(tienda);
        }
    }
    private void listarPedido(){
        vista.dlmPedido.clear();
        ArrayList<Pedido> lista = modelo.getPedidos();
        for(Pedido pedido: lista){
            vista.dlmPedido.addElement(pedido);
        }
    }
    private void listarEmpleado(List<Empleado> lista){
        vista.dlmEmpleado.clear();
        for (Empleado empleado : lista){
            vista.dlmEmpleado.addElement(empleado);
        }
    }
    private void listarTienda(List<Tienda> lista) {
        vista.dlmTienda.clear();
        for (Tienda tienda : lista) {
            vista.dlmTienda.addElement(tienda);
        }
    }
    private void listarCliente(){
        vista.dlmCliente.clear();
        ArrayList<Cliente> lista = modelo.getCliente();
        for(Cliente cliente: lista){
            vista.dlmCliente.addElement(cliente);
        }
    }
    private void listarEmpleado(){
        vista.dlmEmpleado.clear();
        ArrayList<Empleado> lista = modelo.getEmpleado();
        for(Empleado empleado: lista){
            vista.dlmEmpleado.addElement(empleado);
        }
    }


    /**
     * Listo las tiendas en el ComboBox de tiendas
     */
    private void listarTiendaComboBox(ArrayList<Tienda> lista){
        vista.dlmTienda.clear();
        for(Tienda tienda : lista){
            vista.dlmTienda.addElement(tienda);
        }

        vista.TiendaCB.removeAllItems();
        ArrayList<Tienda> ins = modelo.getTienda();

        for (Tienda in : ins){
            vista.TiendaCB.addItem(in);
        }
        vista.TiendaCB.setSelectedIndex(-1);

    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listregalos) {
                mostrarDatosRegalo();
            } else if (e.getSource() == vista.listClientes) {
                mostrarDatosCliente();
            } else if (e.getSource() == vista.listPedidos) {
                mostrarDatosPedido();
            }else if (e.getSource() == vista.listEmpleados) {
            mostrarDatosEmpleado();
            }else if (e.getSource() == vista.listTienda) {
                mostrarDatosTienda();
            }
        }

    }



    /**
     * Metodo que me muestra los datos en los campos de texto, del pedido seleccionado en el JList
     */
    private void mostrarDatosPedido() {
        if(!vista.listPedidos.isSelectionEmpty()){
            Pedido pedido =(Pedido) vista.listPedidos.getSelectedValue();
           vista.preciotxt.setText(String.valueOf(pedido.getPrecio()));
           vista.internet.setSelected(pedido.isInternet());
           vista.pagado.setSelected(pedido.isPagado());
           //listarRegaloComboBox(modelo.getRegalo());
           //listarClienteComboBox(modelo.getCliente());
          // listarEmpleadoComboBox(modelo.getEmpleado());
        }
    }

    /**
     * Metodo que me muestra los datos en los campos de texto, del empleado seleccionado en el JList
     */
    private void mostrarDatosEmpleado() {
        if(!vista.listEmpleados.isSelectionEmpty()){
           Empleado empleado =(Empleado) vista.listEmpleados.getSelectedValue();
           vista.nombreEmpleado.setText(empleado.getNombre());
           vista.fecha_empleado.setDate(empleado.getFechaInicio());
           vista.edadEmpleadoTxt.setText(String.valueOf(empleado.getEdad()));
            listarTiendaComboBox(modelo.getTienda());
        }
    }
    /**
     * Metodo que me muestra los datos en los campos de texto, de la tienda seleccionado en el JList
     */
    private void mostrarDatosTienda() {
        if(!vista.listTienda.isSelectionEmpty()){
            Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
            vista.nombreTienda.setText(tienda.getNombre());
            vista.fecha_apertura.setDate(tienda.getFechaApertura());
            vista.ciudad_tiendaTxt.setText(tienda.getCiudad());
            vista.telefono_tiendaTxt.setText(tienda.getTelefono());
            vista.abierto.setSelected(tienda.isAbierto());
        }
    }
    /**
     * Metodo que me muestra los datos en los campos de texto, del cliente seleccionado en el JList
     */
    public  void mostrarDatosCliente() {
        if (!vista.listClientes.isSelectionEmpty()) {
            Cliente cliente = (Cliente) vista.listClientes.getSelectedValue();
            vista.nombreTxt.setText(cliente.getNombre());
            vista.fecha_Cliente.setDate(cliente.getFechaNacimiento());
            vista.apellidoTxt.setText(cliente.getApellido());
            vista.CiudadTxt.setText(cliente.getCiudad());
            vista.pedido.setSelected(cliente.isPedido());
            listarTiendaComboBox(modelo.getTienda());

        }
    }

    /**
     * Metodo que me muestra los datos en los campos de texto, del regalo seleccionada en el JList
     */
    private void mostrarDatosRegalo(){
        if(!vista.listregalos.isSelectionEmpty()){
           Regalo regalo = (Regalo) vista.listregalos.getSelectedValue();
            vista.tipoTxt.setText(regalo.getTiporegalo());
            vista.espanyolRB.setSelected(regalo.getIdioma().equalsIgnoreCase("espanyol"));
            vista.inglesRB.setSelected(regalo.getIdioma().equalsIgnoreCase("ingles"));
            vista.preciotxt.setText(String.valueOf(regalo.getPrecio()));
            vista.nombreCB.setSelected(regalo.isNombreCB());
            vista.fechaCB.setSelected(regalo.isFechaCB());


        }
    }


    /**
     * Cuando se abre la vista, conecto hibernate y listo los diferentes elementos
     * @param e
     */
    @Override
    public void windowOpened(WindowEvent e) {
        //Le indico que estoy conectando con mysql
        modelo.conectar();
       listarCliente();
       listarEmpleado();
       listarPedido();
       listarRegalo();
       listarTienda();
       listarTiendaComboBox(modelo.getTienda());

    }

    /**
     * Cuando se cierra la vista, desconecto hibernate
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        modelo.desconectar();
    }


    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.buscarEmpleadoTXT){
            listarEmpleado(modelo.getEmpleado(vista.buscarEmpleadoTXT.getText()));
        }
        if(e.getSource() == vista.buscarTiendaTxt){
            listarTienda(modelo.getTienda(vista.buscarTiendaTxt.getText()));
        }
    }
}
