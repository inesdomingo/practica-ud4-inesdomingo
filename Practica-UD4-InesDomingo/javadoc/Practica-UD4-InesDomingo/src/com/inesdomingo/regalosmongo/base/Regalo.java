package com.inesdomingo.regalosmongo.base;

import org.bson.types.ObjectId;

public class Regalo {
    private ObjectId _id;
    private String tiporegalo;
    private String idioma;
    private double precio;
    private boolean fechaCB;
    private boolean nombreCB;

    public Regalo( String tiporegalo, String idioma, float precio, boolean fechaCB, boolean nombreCB) {
        this.tiporegalo = tiporegalo;
        this.idioma = idioma;
        this.precio = precio;
        this.fechaCB = fechaCB;
        this.nombreCB = nombreCB;

    }

    public Regalo(ObjectId _id) {
        this._id = _id;
    }
    public Regalo(){

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getTiporegalo() {
        return tiporegalo;
    }

    public void setTiporegalo(String tiporegalo) {
        this.tiporegalo = tiporegalo;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isFechaCB() {
        return fechaCB;
    }

    public void setFechaCB(boolean fechaCB) {
        this.fechaCB = fechaCB;
    }

    public boolean isNombreCB() {
        return nombreCB;
    }

    public void setNombreCB(boolean nombreCB) {
        this.nombreCB = nombreCB;
    }

    @Override
    public String toString() {
        return
                " tiporegalo='" + tiporegalo + '\'' +
                ", precio=" + precio;
    }
}
