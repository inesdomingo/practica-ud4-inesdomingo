package com.inesdomingo.regalosmongo.base;

import org.bson.types.ObjectId;

import java.sql.Date;
import java.time.LocalDate;

public class Empleado {
    private ObjectId _id;
    private String nombre;
    private int edad;
    private LocalDate fechaInicio;
    private Tienda tienda;

    public Empleado( String nombre, int edad, LocalDate fechaInicio,Tienda tienda) {

        this.nombre = nombre;
        this.edad = edad;
        this.fechaInicio = fechaInicio;
        this.tienda=tienda;

    }

    public Empleado(ObjectId _id) {
        this._id = _id;
    }
    public Empleado(){

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    @Override
    public String toString() {
        return " nombre='" + nombre + '\'' +
                ", edad=" + edad;
    }
}
